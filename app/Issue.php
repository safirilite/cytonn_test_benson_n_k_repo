<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    //
    protected $fillable = [
        'issue',
        'user_id',
        'task_id'
    ];

    protected  $hidden = [
        "created_at",
        "updated_at",
    ];
}
