# Tasks Management

V 0.1


[spencerbenson.com](http://www.spencerbenson.com)

This is a project management system built on Laravel 5.1.* & Vue.js

# Included features are:
  - User management
  - Client management
  - Project management
  - Task management 
  - prioritizing tasks
  - share Projects
  - Api based

# Installation
 - Clone the repo
 - Run composer install
 - Run php artisan migrate
 - Run php artisan db:seed