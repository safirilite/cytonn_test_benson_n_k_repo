@extends('templates.outs.home')

@section('content')
    {{-- HEADER--}}
	<div class="hug hug-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a href="{{ route('home') }}" class="pull-left">Task Management</a>
                    <a href="{{ route('login') }}" class="btn btn-primary btn-line pull-right login">Login</a>
                    <a href="{{ route('register') }}" class="btn btn-primary btn-line pull-right register">Register</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
	</div>

    {{-- HEREO SECTION --}}
    <div class="hug hug-hero">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                        <h1>Task Management System </h1>
                        <h2>By - Benson N. K. </h2>
                        
                        <a href="{{ route('register') }}" class="btn btn-special">Register</a>
                    
                </div>
            </div>
        </div>
    </div>

  

    {{-- footer --}}
    <div class="hug hug-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Version <span class="color-primary">0.1</span></h3>
                    <hr class="special">
                    <p class="text-center last-line">Copyright {{ date("Y") }} &copy;  <a href="https://twitter.com/babi_msoto" target="_blank">Benson N K</a></p>
                </div>
            </div>
        </div>
    </div>
@stop